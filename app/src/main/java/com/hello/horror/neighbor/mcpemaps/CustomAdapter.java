package com.hello.horror.neighbor.mcpemaps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import com.hello.horror.neighbor.mcpemaps.R;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
    private Context mContext;
    private Integer[] mImage;
    private String[] mTitle;
    private String[] msubTitle;



    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView subtitle;
        ImageView imgView;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.title = (TextView) itemView.findViewById(R.id.title);
            this.subtitle = (TextView) itemView.findViewById(R.id.subtitle);
            this.imgView = (ImageView) itemView.findViewById(R.id.imgcar);
        }
    }

    public CustomAdapter(Context mContext, Integer[] image, String[] title, String[] subTitle) {
        this.mContext = mContext;
        this.mImage = image;
        this.mTitle = title;
        this.msubTitle = subTitle;
    }

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent,
                                           final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_layout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int i) {
        holder.title.setText(mTitle[i]);
        holder.subtitle.setText(msubTitle[i]);
        Picasso.with(mContext).load(mImage[i]).into(holder.imgView);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(mContext,"Position : "+i,Toast.LENGTH_LONG).show();
//                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + R.array.appswall));
                if (holder.getAdapterPosition() == 0) {
                    AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setTitle(R.string.titlegp);
                    alertbox.setMessage(R.string.textgp);
                            alertbox.setPositiveButton(R.string.yesgp, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setData(Uri.parse("market://details?id=com.siren.head.scp.mcpemods"));
                                    mContext.startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.nogp, null)
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
//                    String myUrl = "market://details?id=com.vinniemobgr.skyblmaps";


                }
                else if (holder.getAdapterPosition() == 2) {
                    AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setTitle(R.string.titlegp);
                    alertbox.setMessage(R.string.textgp);
                    alertbox.setPositiveButton(R.string.yesgp, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Continue with delete operation
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setData(Uri.parse("market://details?id="));
                            mContext.startActivity(intent);
                        }
                    })
                            .setNegativeButton(R.string.nogp, null)
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                    // mContext.startActivity(myIntent);
                }

                else if (holder.getAdapterPosition() == 1) {
                    AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setTitle(R.string.titlegp);
                    alertbox.setMessage(R.string.textgp);
                    alertbox.setPositiveButton(R.string.yesgp, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Continue with delete operation
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setData(Uri.parse("market://details?id="));
                            mContext.startActivity(intent);
                        }
                    })
                            .setNegativeButton(R.string.nogp, null)
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                    // mContext.startActivity(myIntent);
                }

                else if (holder.getAdapterPosition() == 3) {
                    AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setTitle(R.string.titlegp);
                    alertbox.setMessage(R.string.textgp);
                    alertbox.setPositiveButton(R.string.yesgp, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Continue with delete operation
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setData(Uri.parse("market://details?id="));
                            mContext.startActivity(intent);
                        }
                    })
                            .setNegativeButton(R.string.nogp, null)
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                    // mContext.startActivity(myIntent);
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return mTitle.length;
    }
}