package com.hello.horror.neighbor.mcpemaps;

/**
 * Created by pc on 14-08-2016.
 */
public class Map {
    private String name;
    private int id;
    private String thumbnail;
    private String url;
    private String archive_name;
    private String version;
    private String description;
   // private String version;


    public Map(int id, String name, String version, String description, String url, String thumbnail, String archive_name) {
        this.id = id;
        this.name = name;
        this.version = version;
       // this.version = version;
        this.description = description;
        this.thumbnail = thumbnail;
        this.url = url;
        this.archive_name = archive_name;
    }

    public String getName() {
        return name;
    }
    public String getVersion() {
        return version;
    }

    public int getId() {
        return id;
    }
    public String getDescription() {
        return description;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setVersion(String version) {
        this.name = version;
    }
    public String getUrl() {
        return url;
    }
    public String getArchiveName() {
        return archive_name;
    }


    public String getThumbnail() {
        return thumbnail;
    }
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
