package com.hello.horror.neighbor.mcpemaps;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kobakei.ratethisapp.RateThisApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hello.horror.neighbor.mcpemaps.R;
import com.hello.horror.neighbor.mcpemaps.util.PreferenceUtil;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import dmax.dialog.SpotsDialog;
//import test.galardost.ru.util.PreferenceUtil;


public class MainActivity extends AppCompatActivity implements MapsAdapter.OnCardClickListener {
    private static final String TAG = "HttpDownloadManager";
    private ProgressDialog pDialog;
    private RecyclerView recyclerView;
    private MapsAdapter adapter;
    public static final int APP_INTRO_REQUEST = 100;
    private List<Map> mapList;
    private static final String[] PERMISSIONS =
            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private boolean blockRequestPermissions;
    private Map map;
    public static final String APP_PREFERENCES = "settings";
    public static final String APP_PREFERENCES_FIRST = "first";
    private SharedPreferences mSettings;

    public String url = "https://api.galapp.ru/api/v1/items?token=ZXV8Fo8PwYIQigE7sxjT";
    public static final String PREF_KEY_FIRST_START = "com.vinniesmob.lucky.PREF_KEY_FIRST_START";
    public static final int REQUEST_CODE_INTRO = 1;
    private StartAppAd startAppAd = new StartAppAd(this);

    FloatingActionButton fab, fab1, fab2, fab3;
    LinearLayout fabLayout1, fabLayout2, fabLayout3;
    View fabBGLayout;
    boolean isFABOpen = false;
    ProgressDialog progress;
    Dialog mDialog;
    private int strokeColor= Color.TRANSPARENT;
    private int strokeWidth=2;


 //   private ActivityFinishBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StartAppSDK.init(this, "210295893", false);
        startAppAd.showAd();
        RateThisApp.showRateDialogIfNeeded(MainActivity.this);

        if (!MainActivity.this.isFinishing() && progress != null) {
            progress.dismiss();
        }

        if (!MainActivity.this.isFinishing() && mDialog != null) {
            mDialog.dismiss();
        }



        if (!MainActivity.this.isFinishing() && pDialog != null) {
            pDialog.dismiss();
        }

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if (mSettings.contains(APP_PREFERENCES_FIRST) && mSettings.getInt(APP_PREFERENCES_FIRST, 0) == 1) {

        } else {
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putInt(APP_PREFERENCES_FIRST, 1);
            editor.apply();
          //  Intent intent = new Intent(MainActivity.this, IntroActivity.class);
           // startActivity(intent);
        }




        boolean firstStart = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(PREF_KEY_FIRST_START, true);

        if (firstStart) {
            Intent intent = new Intent(this, AppIntroActivity.class);
            startActivityForResult(intent, REQUEST_CODE_INTRO);

        }

        if(Locale.getDefault().getLanguage() != "ru") {
            url = url + "&lang=en";
        }





        setContentView(com.hello.horror.neighbor.mcpemaps.R.layout.activity_main);




        if (isFirstTime())  {

            if (mDialog == null) {
                mDialog = new SpotsDialog.Builder()
                        .setContext(MainActivity.this)
                        .setMessage(R.string.refr)
                        .setCancelable(false)
                        .build();

            }

            mDialog.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mDialog.dismiss();

                    // RateThisApp.showRateDialogIfNeeded(MainActivity.this);

                }
            }, 1200);
        }

        else {
//            final Dialog mDialog;
//            mDialog = new SpotsDialog.Builder()
//                    .setContext(MainActivity.this)
//                    .setMessage(R.string.refr)
//                    .setCancelable(false)
//                    .build();
//            mDialog.show();
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                public void run() {
//                    mDialog.dismiss();
//                    RateThisApp.showRateDialogIfNeeded(MainActivity.this);
//                }
//            }, 1000);


            //final ProgressDialog progress = new ProgressDialog(this);

            if (progress == null) {
                progress = new ProgressDialog(MainActivity.this);
                progress.setTitle(R.string.load);
                progress.setCancelable(false);
                progress.setMessage(getText(R.string.wait));

            }

            progress.show();


            Runnable progressRunnable = new Runnable() {

                @Override
                public void run() {

                    RateThisApp.showRateDialogIfNeeded(MainActivity.this);


                    startAppAd.showAd();
                    progress.cancel();


                }
            };

            Handler pdCanceller = new Handler();

            pdCanceller.postDelayed(progressRunnable, 2000);
        }




        RateThisApp.onCreate(this);
        RateThisApp.Config config = new RateThisApp.Config(0, 5);
        config.setTitle(R.string.my_own_title);
        config.setMessage(R.string.my_own_message2);
        config.setYesButtonText(R.string.my_own_rate);
        config.setNoButtonText(R.string.my_own_thanks);
        config.setCancelButtonText(R.string.my_own_cancel);
        RateThisApp.init(config);
//
//        if (!EasyPermissions.hasPermissions(this, PERMISSIONS)) {
//            EasyPermissions.requestPermissions(this, getBaseContext().getString(R.string.permission), 0x01,
//                    PERMISSIONS);
//            return;
//        }

        initCollapsingToolbar();

        recyclerView = (RecyclerView) findViewById(com.hello.horror.neighbor.mcpemaps.R.id.recycler_view);

        mapList = new ArrayList<>();
        adapter = new MapsAdapter(this, mapList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.setOnCardClickListener(this);

        new GetMaps().execute();

        try {
            Glide.with(this).load(R.drawable.backdrop).into((ImageView) findViewById(com.hello.horror.neighbor.mcpemaps.R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }






        final BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
//        final Menu menu = bottomNavigationView.getMenu();

//       menu.findItem(R.id.action_recents).setIcon(R.drawable.star_empty);
        // menu.findItem(R.id.action_favorites).setIcon(R.drawable.ic_star_border_black_24dp);
        int size = bottomNavigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            bottomNavigationView.getMenu().getItem(i).setCheckable(false);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int size = bottomNavigationView.getMenu().size();
                for (int i = 0; i < size; i++) {
                    bottomNavigationView.getMenu().getItem(i).setCheckable(true);
                }
                switch (item.getItemId()) {
                    case R.id.action_recents:


                        // Toast.makeText(MainActivity.this, "Recents", Toast.LENGTH_SHORT).show();
                        //   RateThisApp.showRateDialogIfNeeded(MainActivity.this);
                        RateThisApp.showRateDialog(MainActivity.this);

//                        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
//                                "://" + getResources().getResourcePackageName(coverImageView.getId())
//                                + '/' + "drawable" + '/' + getResources().getResourceEntryName((int)coverImageView.getTag()));
//



                        break;
                    case R.id.action_favorites:
                        //    Toast.makeText(MainActivity.this, "Favorites", Toast.LENGTH_SHORT).show();

                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_TEXT,
                                getText(R.string.shareapp) + BuildConfig.APPLICATION_ID);
                        shareIntent.setType("text/plain");
                        startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));

                        break;
                    case R.id.action_nearby:
                        // Toast.makeText(MainActivity.this, "Nearby", Toast.LENGTH_SHORT).show();
                        MaterialStyledDialog dialog = new MaterialStyledDialog.Builder(MainActivity.this)
                                .setTitle(R.string.info)
                                .setPositiveText(R.string.gotit)
                                .setCancelable(false)
                                .setIcon(R.mipmap.ic_launcher)
                                .setDescription(R.string.infofull)
                                .build();
                        dialog.show();

//
//                        Intent intent = new Intent(MainActivity.this, AppsActivity.class);
//                        startActivity(intent);
                        break;
                }
                return true;
            }
        });

    }

    private void dismissProgressDialog() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    private void dism() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    private void disma() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        dism();
        disma();
        super.onDestroy();
    }


    private boolean isFirstTime()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }

        @Override
        public void onBackPressed() {
            AlertDialog.Builder exitbox = new AlertDialog.Builder(MainActivity.this);
            exitbox.setTitle(R.string.exittitle);
            exitbox.setMessage(R.string.exitsure);
            exitbox.setPositiveButton(R.string.yesgp, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Continue with delete operation
                    finish();

                }
            }) .setNegativeButton(R.string.nogp, null)
                    .setIcon(android.R.drawable.ic_menu_close_clear_cancel)
                    .show();

         //   super.onBackPressed();

    }




    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(com.hello.horror.neighbor.mcpemaps.R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(com.hello.horror.neighbor.mcpemaps.R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(com.hello.horror.neighbor.mcpemaps.R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }






    @Override
    public void onCardClick(View view, int position) {

        map = mapList.get(position);

        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("item_id", map.getId());
        startActivity(intent);
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount;

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount;
                outRect.right = (column + 1) * spacing / spanCount;

                if (position < spanCount) {
                    outRect.top = spacing;
                }
                outRect.bottom = spacing;
            } else {
                outRect.left = column * spacing / spanCount;
                outRect.right = spacing - (column + 1) * spacing / spanCount;
                if (position >= spanCount) {
                    outRect.top = spacing;
                }
            }
        }
    }


    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetMaps extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage(getBaseContext().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(url);

            if (jsonStr != null) {
                try {
                    JSONArray maps = new JSONArray(jsonStr);

                    for (int i = 0; i < maps.length(); i++) {
                        JSONObject c = maps.getJSONObject(i);

                            int id = c.getInt("id");
                            String name = c.getString("name");
                            String description = c.getString("description");
                            String archive = c.getString("archive");
                            String image = c.getString("image");
                            String version = getString(R.string.ver) + c.getString("version");

                            Map map = new Map(id, name, version, description, archive, image, "new-island.mcworld");
                            mapList.add(map);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            recyclerView.setAdapter(adapter);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_INTRO) {
            if (resultCode == RESULT_OK) {
                PreferenceManager.getDefaultSharedPreferences(this).edit()
                        .putBoolean(PREF_KEY_FIRST_START, false)
                        .apply();
            } else {
                PreferenceManager.getDefaultSharedPreferences(this).edit()
                        .putBoolean(PREF_KEY_FIRST_START, true)
                        .apply();
                //User cancelled the intro so we'll finish this activity too.
                finish();
            }
        }
    }

//    @Override
//    protected void requestPermissions() {
//        if (!blockRequestPermissions) super.requestPermissions();
//    }


    private boolean checkShowIntro() {
        if (!PreferenceUtil.getInstance(this).introShown()) {
            PreferenceUtil.getInstance(this).setIntroShown();
            blockRequestPermissions = true;
            new Handler().postDelayed(() ->

                            startActivityForResult(new Intent(MainActivity.this, AppIntroActivity.class), APP_INTRO_REQUEST)

                    , 50);
            //new Handler().postDelayed(() -> RateThisApp.showRateDialogIfNeeded(MainActivity.this), 10000);

            final ProgressDialog progresssss = new ProgressDialog(MainActivity.this);
            progresssss.setTitle("Загрузка...");
            progresssss.setCancelable(false);
            progresssss.setMessage("Пожалуйста, подождите");

            progresssss.show();

            Runnable progresssssRunnable = new Runnable() {

                @Override
                public void run() {
                  //  RateThisApp.showRateDialogIfNeeded(MainActivity.this);
                    progresssss.cancel();

                }
            };

            Handler pdCancellers = new Handler();

            pdCancellers.postDelayed(progresssssRunnable, 4000);


            return true;
        }


        return false;
    }

}