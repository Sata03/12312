package com.hello.horror.neighbor.mcpemaps.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import android.preference.PreferenceManager;

import androidx.annotation.NonNull;


public final class PreferenceUtil {



    public static final String INTRO_SHOWN = "intro_shown";


    private static PreferenceUtil sInstance;

    private final SharedPreferences mPreferences;

    private PreferenceUtil(@NonNull final Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static PreferenceUtil getInstance(@NonNull final Context context) {
        if (sInstance == null) {
            sInstance = new PreferenceUtil(context.getApplicationContext());
        }
        return sInstance;
    }




    @SuppressLint("CommitPrefEdits")
    public void setIntroShown() {
        // don't use apply here
        mPreferences.edit().putBoolean(INTRO_SHOWN, true).commit();
    }

    public final boolean introShown() {
        return mPreferences.getBoolean(INTRO_SHOWN, false);
    }




}
