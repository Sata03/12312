package com.hello.horror.neighbor.mcpemaps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;


import com.bumptech.glide.Glide;
import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Logger;
import com.coolerfall.download.OkHttpDownloader;
import com.coolerfall.download.Priority;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import androidx.core.content.FileProvider;

import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;
import pub.devrel.easypermissions.EasyPermissions;


import com.kobakei.ratethisapp.RateThisApp;
import com.hello.horror.neighbor.mcpemaps.R;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

public class DetailActivity extends AppCompatActivity {

    public Map map;
    private static final String[] PERMISSIONS =
            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private DownloadManager downloadManager;
    private String token = "ZXV8Fo8PwYIQigE7sxjT";
    public String url = "https://api.galapp.ru/api/v1/items/";
    public int map_id;
    private ProgressDialog pDialog;
    public TextView name;
    public TextView version;
    public TextView description;
    public ImageView thumbnail;
   // public TextView author;
    private ProgressBar progressBar;
    private Button button;
    private String btn_status;
    androidx.appcompat.app.ActionBar actionBar;
    Dialog mDialog;
    Dialog rDialog;
    private StartAppAd startAppAd = new StartAppAd(this);




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, "210295893", false);
        startAppAd.showAd();




        setContentView(R.layout.activity_detail);


        if (!DetailActivity.this.isFinishing() && mDialog != null) {
            mDialog.dismiss();
        }

        if (!DetailActivity.this.isFinishing() && rDialog != null) {
            rDialog.dismiss();
        }




//       Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        thumbnail = findViewById(R.id.thumbnail);
        button = findViewById(R.id.button);
        version = findViewById(R.id.version);
        progressBar = findViewById(R.id.progressBar);
     //   author = findViewById(R.id.author);

        map_id = getIntent().getExtras().getInt("item_id");
        url = url + map_id + "?token=" + token;

        if(Locale.getDefault().getLanguage() != "ru") {
            url = url + "&lang=en";
        }

        new GetMap().execute();

       // author.setMovementMethod(LinkMovementMethod.getInstance());
        description.setMovementMethod(LinkMovementMethod.getInstance());

        OkHttpClient client = new OkHttpClient.Builder().build();
        downloadManager = new DownloadManager.Builder().context(this)
                .downloader(OkHttpDownloader.create(client))
                .threadPoolSize(3)
                .logger(new Logger() {
                    @Override
                    public void log(String message) {
                        Log.d("TAG", message);
                    }
                })
                .build();



        if (!EasyPermissions.hasPermissions(this, PERMISSIONS)) {
            EasyPermissions.requestPermissions(this, getBaseContext().getString(R.string.permission), 0x01,
                    PERMISSIONS);


            return;
        }

        if (isFirstTime())  {



            if (mDialog == null) {
                mDialog = new SpotsDialog.Builder()
                        .setContext(DetailActivity.this)
                        .setMessage(R.string.refr)
                        .setCancelable(false)
                        .build();

            }

            mDialog.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    startAppAd.showAd();
                    mDialog.dismiss();


                }
            }, 1200);
        }

        else {

            Random rand = new Random();
            int chance = rand.nextInt(10);
            Log.d(getClass().getName(), "value = " + chance);
            // Log.i("value entered", Float.toString(chance));
            if (chance > 7)  {

                if (mDialog == null) {
                    mDialog = new SpotsDialog.Builder()
                            .setContext(DetailActivity.this)
                            .setMessage(R.string.refr)
                            .setCancelable(false)
                            .build();

                }

                mDialog.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        startAppAd.showAd();

                        mDialog.dismiss();

                    }
                }, 1200);
            }
        }

        RateThisApp.onCreate(this);


        RateThisApp.Config config = new RateThisApp.Config(0, 5);
        config.setTitle(R.string.my_own_title);
        config.setMessage(R.string.my_own_message2);
        config.setYesButtonText(R.string.my_own_rate);
        config.setNoButtonText(R.string.my_own_thanks);
        config.setCancelButtonText(R.string.my_own_cancel);
        RateThisApp.init(config);

//



//
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                // app icon in action bar clicked; go home
//                Intent intent = new Intent(this, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }



    private void dism() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    private void disma() {
        if (rDialog != null && rDialog.isShowing()) {
            rDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        dism();
        disma();
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }


    private boolean isFirstT()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }

    public void download_click(View view) {

        if(btn_status == "play") {
            String file_type = null;

            String[] supportedMimeTypes = {"application/mcpack","application/mcworld", "application/mcaddon"};

            File file = new File(map.getArchiveName());
            Uri uri = FileProvider.getUriForFile(DetailActivity.this,
                    com.hello.horror.neighbor.mcpemaps.BuildConfig.APPLICATION_ID + ".provider",
                    file);

            if(map.getUrl().contains("mcaddon")) {
                file_type = "application/mcaddon";
            } else if(map.getUrl().contains("mcworld")) {
                file_type = "application/mcworld";
            } else if(map.getUrl().contains("mcpack")) {
                file_type = "application/mcpack";
            }


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setPackage("com.mojang.minecraftpe");

            intent.setDataAndType(uri, file_type);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);




            try {


                //startActivity(chooserIntent);
                final Dialog mDialog;
                mDialog = new SpotsDialog.Builder()
                        .setContext(DetailActivity.this)
                        .setMessage(R.string.instrun)
                        .setCancelable(false)
                        .build();
                mDialog.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mDialog.dismiss();

                    }
                }, 3000);

                startActivity(intent);



            } catch ( final Exception e ) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
                builder.setTitle(R.string.error)
                        .setMessage(R.string.error_content)
                        .setCancelable(false)
                        .setNegativeButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }

        if (!EasyPermissions.hasPermissions(this, PERMISSIONS)) {
            EasyPermissions.requestPermissions(this, getBaseContext().getString(R.string.permission), 0x01,
                    PERMISSIONS);
            return;
        }

        String destPath = Environment.getExternalStorageDirectory() + "/Download" + File.separator + map.getUrl().substring(map.getUrl().lastIndexOf('/')+1);

        DownloadRequest request = new DownloadRequest.Builder().url(map.getUrl())
                .downloadCallback(new Callback())
                .retryTime(3)
                .retryInterval(3, TimeUnit.SECONDS)
                .progressInterval(1, TimeUnit.SECONDS)
                .priority(Priority.HIGH)
                .destinationFilePath(destPath)
                .allowedNetworkTypes(DownloadRequest.NETWORK_MOBILE)
                .build();

        downloadManager.add(request);
    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetMap extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DetailActivity.this);
            pDialog.setMessage(getBaseContext().getString(R.string.loading));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(url);

            if (jsonStr != null) {
                try {
                    JSONObject c = new JSONObject(jsonStr);

                    int id = c.getInt("id");
                    String name = c.getString("name");
                    String description = c.getString("description");
                    String version = c.getString("version");
                    String archive = c.getString("archive");
                //    String version = getString(R.string.ver) + c.getString("version");
                    String image = c.getString("image");
                    String destPath = Environment.getExternalStorageDirectory() + "/Download" + File.separator + archive.substring(archive.lastIndexOf('/')+1);

                    map = new Map(id, name, version, description, archive, image, destPath);

                } catch (final JSONException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            name.setText(map.getName());
            version.setText(map.getVersion());
           // version.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_letter_m, 0, 0, 3);
            description.setText(map.getDescription());
            Glide.with(DetailActivity.this).load(map.getThumbnail()).into(thumbnail);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                description.setText(Html.fromHtml(map.getDescription(), Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
            } else {
                description.setText(Html.fromHtml(map.getDescription())); }
        }
    }

    private class Callback extends DownloadCallback {

        @Override
        public void onStart(int downloadId, long totalBytes) {
            button.setText(R.string.installation);
            button.setClickable(false);
            button.setBackground(getDrawable(R.drawable.buttinact));
            button.setActivated(false);
            button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_waiting_svgrepo_com, 0, 0, 0);
        }

        @Override
        public void onRetry(int downloadId) {

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
            int progress = (int) (bytesWritten * 100f / totalBytes);
            progress = progress == 100 ? 0 : progress;
            progressBar.setProgress(progress);
        }

        @Override
        public void onSuccess(int downloadId, String filePath) {
            progressBar.setVisibility(View.INVISIBLE);
            button.setText(R.string.popup_btn_text);
            btn_status = "play";
            button.setClickable(true);
            button.setBackground(getDrawable(R.drawable.button));
            button.setCompoundDrawablePadding(20);
            button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_round_done_button_svgrepo_com, 0, 0, 0);
             button.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            //button.setPadding(20, 20, 40, 20);
            pDialog.dismiss();

            Random rand = new Random();
            int chance = rand.nextInt(10);
            Log.d(getClass().getName(), "value = " + chance);
           // Log.i("value entered", Float.toString(chance));
            if (isFirstT()) {
                if (chance > 8) {

                    if (rDialog == null) {
                        rDialog = new SpotsDialog.Builder()
                                .setContext(DetailActivity.this)
                                .setMessage(R.string.refr)
                                .setCancelable(false)
                                .build();

                    }

                    rDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                          //  Appodeal.show(DetailActivity.this, Appodeal.INTERSTITIAL);

                            rDialog.dismiss();

                        }
                    }, 1200);
                }
            }

            else {
                if (chance > 5) {

                    if (rDialog == null) {
                        rDialog = new SpotsDialog.Builder()
                                .setContext(DetailActivity.this)
                                .setMessage(R.string.refr)
                                .setCancelable(false)
                                .build();

                    }

                    if(!((Activity) DetailActivity.this).isFinishing())
                    {
                        rDialog.show();
                    }

                    else if (!DetailActivity.this.isFinishing()){
                        rDialog.show();
                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                          //  Appodeal.show(DetailActivity.this, Appodeal.INTERSTITIAL);

                            rDialog.dismiss();

                        }
                    }, 1200);
                }
            }
        }

        @Override
        public void onFailure(int downloadId, int statusCode, String errMsg) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
//            builder.setTitle(R.string.error)
//                    .setMessage(errMsg)
//                    .setCancelable(false)
//                    .setNegativeButton("OK",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//            AlertDialog alert = builder.create();
//            alert.show();
        }
    }

    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }
}
