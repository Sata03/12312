package com.hello.horror.neighbor.mcpemaps;

import android.animation.ObjectAnimator;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.hello.horror.neighbor.mcpemaps.databinding.ActivitySplashBinding;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;


public class SplashActivity extends AppCompatActivity {


    ProgressBar splashProgress;
    DilatingDotsProgressBar mDilatingDotsProgressBar;
    int SPLASH_TIME = 2000; //This is 3 seconds

    private final Handler waitHandler = new Handler();
    private final Runnable waitCallback = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    };

    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        //This is additional feature, used to run a progress bar
        splashProgress = findViewById(R.id.splashProgress);
        playProgress();

        //   mDilatingDotsProgressBar = (DilatingDotsProgressBar) findViewById(R.id.progress);
        // mDilatingDotsProgressBar.showNow();
        splashProgress.setVisibility(View.VISIBLE);
        splashProgress.setProgressDrawable(getDrawable(R.drawable.progressdrawable));

        //Fake wait 2s to simulate some initialization on cold start (never do this in production!)
    //    waitHandler.postDelayed(waitCallback, 2000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do any action here. Now we are moving to next page
                Intent mySuperIntent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(mySuperIntent);

                //This 'finish()' is for exiting the app when back button pressed from Home page which is ActivityHome
                finish();

            }
        }, SPLASH_TIME);
    }

    private void playProgress() {
        ObjectAnimator.ofInt(splashProgress, "progress", 100)
                .setDuration(2500)
                .start();
    }

    @Override
    protected void onDestroy() {
      //  waitHandler.removeCallbacks(waitCallback);
        super.onDestroy();
    }
}