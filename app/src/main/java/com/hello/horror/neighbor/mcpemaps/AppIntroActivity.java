package com.hello.horror.neighbor.mcpemaps;

import android.Manifest;
import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

import com.hello.horror.neighbor.mcpemaps.R;


public class AppIntroActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // setButtonCtaVisible(true);
        setButtonNextVisible(true);
        setButtonBackVisible(true);

        setButtonCtaTintMode(BUTTON_CTA_TINT_MODE_TEXT);

        addSlide(new SimpleSlide.Builder()
                .title(R.string.welc)
                .description(R.string.welcome_to_app)
                .image(R.drawable.intro1)
                .background(R.color.colorPrimarySplash)
                .backgroundDark(R.color.colorPrimaryDarkSplash)
                .layout(R.layout.fragment_simple_slide_large_image)
                // .permission(Manifest.permission.WRITE_EXTERNAL_STORAGE)

                .build());
        addSlide(new SimpleSlide.Builder()
                .title(R.string.instguide)
                .description(R.string.secondins)
                .image(R.drawable.intro1)
                .buttonCtaLabel(R.string.mi_label_button_new)
                .buttonCtaLabel(R.string.mi_label_button_new)
                .buttonCtaLabel(R.string.mi_label_button_new)
                .background(R.color.colorPrimarySplash)
                .backgroundDark(R.color.colorPrimaryDarkSplash)
                .layout(R.layout.fragment_simple_slide_large_image)
                .permission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .build());

    }
}
