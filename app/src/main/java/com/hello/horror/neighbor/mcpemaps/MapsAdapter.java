package com.hello.horror.neighbor.mcpemaps;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class MapsAdapter extends RecyclerView.Adapter<MapsAdapter.MyViewHolder> {
    private Context mContext;
    private List<Map> mapList;
    private static OnCardClickListener mListener;

    interface OnCardClickListener {
        void onCardClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView title;
        ImageView thumbnail;
        View view;

        Button install_btn;

        public MyViewHolder(View view) {
            super(view);
            this.view = view;

            view.setOnClickListener(this);

            title = view.findViewById(com.hello.horror.neighbor.mcpemaps.R.id.title);
            thumbnail = view.findViewById(com.hello.horror.neighbor.mcpemaps.R.id.thumbnail);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mListener.onCardClick(v, position);
        }
    }


    public void setOnCardClickListener(OnCardClickListener listener) {
        mListener = listener;
    }


    public MapsAdapter(Context mContext, List<Map> mapList) {
        this.mContext = mContext;
        this.mapList = mapList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.hello.horror.neighbor.mcpemaps.R.layout.album_card, parent, false);

        return new MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Map map = mapList.get(position);
        holder.title.setText(map.getName());


        Glide.with(mContext).load(map.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return mapList.size();
    }
}
